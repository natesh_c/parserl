%%%-------------------------------------------------------------------
%%% @author regupathy.b
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Apr 2016 11:20 AM
%%%-------------------------------------------------------------------
-module(parserl).
-author("tringapps").
-include("http.hrl").
-export([decode_stream/3,encode/2,decode/2]).

-callback build(DataSet::term()) -> {ok,Data::binary()} | {wrong_data,Reason::any()}.

-callback scan(Source::binary(),State::term()) -> {incomplete,State::term()} | {complete,DataSet::term()} | {wrong_data,Reason::any()}.

%%%===================================================================
%%% APIs
%%%===================================================================

decode(DataType,Source) -> decode_stream(DataType,Source,[]).

encode(json,Source) -> json_encoder:build(Source);
encode(http,Source) -> http_encoder:build(Source);
encode(xml,Source) -> xml_encoder:build(Source);
encode(_,Source) -> {wrong_data,Source}.

decode_stream(http,Source,State) -> http_decoder:scan(Source,State);
decode_stream(json,Source,State) -> json_decoder:scan(Source,State);
decode_stream(xml,Source,State) -> xml_decoder:scan(Source,State);
decode_stream(redis,Source,State) -> redis_decode:scan(Source,State);
decode_stream(ws,Source,State) -> websocket_decoder:scan(Source,State);
decode_stream(#content_type.json,Source,State) -> json_decoder:scan(Source,State);
decode_stream(#content_type.xml,Source,State) -> xml_decoder:scan(Source,State);
decode_stream(_,Source,_State) -> {wrong_data,Source}.


