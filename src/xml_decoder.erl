%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Apr 2016 11:38 AM
%%%-------------------------------------------------------------------
-module(xml_decoder).
-author("tringapps").

%% API
-export([scan/2]).

-spec scan(Source::binary(),State::term()) -> {incomplete,State::term()} | {complete,DataSet::term()} | {wrong_data,Reason::any()}.

scan(Source,_State) -> {incomplete,Source}.

