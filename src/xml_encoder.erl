%%%-------------------------------------------------------------------
%%% @author tringapps
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Apr 2016 11:38 AM
%%%-------------------------------------------------------------------
-module(xml_encoder).
-author("tringapps").

%% API
-export([build/1]).

-spec build(DataSet::term()) -> {ok,Data::binary()} | {wrong_data,Reason::any()}.
build(DataSet) -> {ok,DataSet}.